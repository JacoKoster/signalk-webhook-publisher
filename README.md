# SignalK Webhook Publisher

This plugin sends periodically status information from SignalK to a chosen URL. Currently supported default keys are:

- environment.depth.belowTransducer
- navigation.position
- navigation.state
- navigation.speedOverGround
- navigation.courseOverGroundTrue
- navigation.speedThroughWater
- environment.water.temperature
- environment.wind.speedApparent
- environment.wind.angleApparent
- environment.pressure
- environment.inside.temperature
- environment.outside.temperature
- environment.inside.humidity
- environment.outside.humidity
- navigation.anchor.position
- navigation.anchor.maxRadius

## Note
If there is no network connection to the chosen URL the message will be dropped and not buffered.