const SUBMIT_INTERVAL = 10;
const axios = require('axios');
var lastUpdate = null;

const KEYS = {
  depth: 'environment.depth.belowTransducer',
  position: 'navigation.position',
  state: 'navigation.state',
  sog: 'navigation.speedOverGround',
  cog: 'navigation.courseOverGroundTrue',
  stw: 'navigation.speedThroughWater',
  waterTemp: 'environment.water.temperature',
  windSpeed : 'environment.wind.speedApparent',
  windDirection: 'environment.wind.angleApparent',
  pressure : 'environment.pressure',
  insideTemperature : 'environment.inside.temperature',
  outsideTemperature: 'environment.outside.temperature',
  insideHumidity: 'environment.inside.humidity',
  outsideHumidity: 'environment.outside.humidity',
  anchorPosition: 'navigation.anchor.position',
  anchorRadius: 'navigation.anchor.maxRadius'
}

const mediumTime = new Intl.DateTimeFormat("en", {
  timeStyle: "medium",
  dateStyle: "short",
});

module.exports = function (app) {
  let submitProcess;
  let plugin = {
    description: 'A SignalK plugin that publishes data to a webhook url',
    id: 'webhook-publisher',
    name: 'SignalK Webhook Publisher'
  };

  plugin.start = function (options) {
    const interval = options.interval || SUBMIT_INTERVAL;

    if (options.url == null) {
      app.setPluginError('Webhook Plugin not started. Webhook URL is not set.');
      app.debug('Webhook url not set. Not started');
      return;
    }
    app.setPluginStatus('Webhook Plugin started. Waiting for first status update.');

    submitProcess = setInterval( function() {
      submitDataToServer(options);
    }, interval * 60 * 1000);
  };

  plugin.stop = function () {
    clearInterval(submitProcess);
    app.setPluginStatus('Webhook Plugin stopped.');
  };

  plugin.schema = {
    type: 'object',
    required: ['url'],
    properties: {
      url: {
        type: "string",
        title: "URL where the webhook posts to"
      },
      token: {
        type: "string",
        title: "Authentication token"
      },
      interval: {
        type: "number",
        title: "Interval in minutes to post to webhook",
        default: SUBMIT_INTERVAL
      },
      updateMoored: {
        type: "boolean",
        title: "Send updates when moored",
        default: true
      }
    }
  };

  function getKeyValue(key) {
    const data = app.getSelfPath(key);
    if (data == null) {
      return null;
    }

    return {
      value: data.value,
      unit: data.meta?.units
    }
  }

  function submitDataToServer(options) {
    const data = {
      meta: {
        name: app.getSelfPath('name'),
        mmsi: app.getSelfPath('mmsi'),
        length: app.getSelfPath('design.length.value.overall'),
        beam: app.getSelfPath('design.beam.value'),
        height: app.getSelfPath('design.airHeight.value'),
        ship_type: app.getSelfPath('design.aisShipType.value.id'),
        signalk_version: app.config.version
      },
      position: getKeyValue(KEYS.position),
      state: getKeyValue(KEYS.state),
      sog: getKeyValue(KEYS.sog),
      cog: getKeyValue(KEYS.cog),
      stw: getKeyValue(KEYS.stw),
      water: {
        depth: getKeyValue(KEYS.depth),
        temperature: getKeyValue(KEYS.waterTemp)
      },
      wind: {
        speed: getKeyValue(KEYS.windSpeed),
        direction: getKeyValue(KEYS.windDirection)
      },
      pressure: getKeyValue(KEYS.pressure),
      temperature: {
        inside: getKeyValue(KEYS.insideTemperature),
        outside: getKeyValue(KEYS.outsideTemperature)
      },
      humidity: {
        inside: getKeyValue(KEYS.insideHumidity),
        outside: getKeyValue(KEYS.outsideHumidity)
      },
      anchor: {
        position: getKeyValue(KEYS.anchorPosition),
        radius: getKeyValue(KEYS.anchorRadius)
      }
    }

    const ts = new Date();
    const theTime = mediumTime.format(ts);

    // Don't send an update when moored
    if(options.updateMoored === false && data.state.value === "moored" && lastUpdate?.state?.value === "moored") {
      app.setPluginStatus(`${theTime} : Detected ship is moored, skipping update`);
      return;
    }

    lastUpdate = data;

    let config = {
      method: 'post',
      url: options.url,
      headers: {
        'X-uuid': app.getSelfPath('uuid'),
        'Content-Type': 'application/json'
      },
      data : data
    };

    if(options.token) {
      config.headers.Authorization = `Bearer ${options.token}`
    }

    app.debug(`Sending ${JSON.stringify(data)}`);

    axios.request(config)
      .then(() => {
        app.setPluginStatus(`${theTime} : Successfully sent data to server`);
        app.debug('Data successfully submitted');
      })
      .catch(() => {
        app.setPluginError(`${theTime} : Posting to server url failed`);
        app.debug(`Posting to server url failed`);
      });
  }

  return plugin;
};